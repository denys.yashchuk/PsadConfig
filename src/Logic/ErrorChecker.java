package Logic;

/**
 * Consists methods for checking if the parameters values is correct
 */
public class ErrorChecker {

    /**
     * Checks is the String number String
     *
     * @param input - String for checking
     * @return true - if String is number String
     * false - otherwise
     */
    public static boolean numberChecker(String input) {
        return input.matches("\\d+");
    }

    /**
     * Checks is the String "Y" or "N"
     *
     * @param input - String for checking
     * @return true - if String is "Y" or "N"
     * false - otherwise
     */
    public static boolean yesNoChecker(String input) {
        return input.equals("Y") || input.equals("N");
    }

    /**
     * Checks is the String number from 1 to 5
     *
     * @param input - String for checking
     * @return true - if String is number from 1 to 5
     * false - otherwise
     */
    public static boolean numberTo5(String input) {
        return numberChecker(input) && Integer.parseInt(input) <= 5;
    }

}
