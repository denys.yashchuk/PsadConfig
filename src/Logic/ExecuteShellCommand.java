package Logic;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Consists methods for executing shell commands
 */
public class ExecuteShellCommand {

    /**
     * Executes "systemctl status psad" command
     *
     * @return psad status
     * @throws Exception
     */
    public static String getPsadStatus() throws Exception {

        String output = "";

        Process p;
        p = Runtime.getRuntime().exec("systemctl status psad");
        p.waitFor();
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null) {
            if (line.trim().startsWith("Active:"))
                return line.trim().split(" ")[1];
        }

        return output;

    }

    /**
     * Executes "systemctl stop psad" command
     *
     * @return output after command executing
     * @throws Exception
     */
    public static boolean stopPsad() {
        String output = "";

        Process p;
        try {
            p = Runtime.getRuntime().exec("systemctl stop psad");
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output += line;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.trim().isEmpty();
    }

    /**
     * Executes "systemctl start psad" command
     *
     * @return output after command executing
     * @throws Exception
     */
    public static boolean startPsad() {
        String output = "";

        Process p;
        try {
            p = Runtime.getRuntime().exec("systemctl start psad");
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output += line;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.trim().isEmpty();
    }

}
