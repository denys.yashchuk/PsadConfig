package FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Reads and parsing /etc/psad/psad.conf file with configs
 */
public class ReadConfig {

    // String with other configs
    private static String otherConfigs;

    // Map with parameters, which can be changed in GUI
    private static HashMap<String, String> out;

    public static void readFile() throws FileNotFoundException {
        if (out == null) {
            String s;
            boolean other = false;
            otherConfigs = "";
            out = new HashMap<String, String>();
            File file = new File("/etc/psad/psad.conf");
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                s = sc.nextLine();
                if (!other) {
                    if (s.length() < 1) {
                        other = true;
                        continue;
                    }
                    String[] conf = s.split("\t");
                    out.put(conf[0], conf[1].substring(0, conf[1].length() - 1));
                } else {
                    otherConfigs += (sc.hasNextLine() ? s + "\n" : s);
                }
            }
            System.out.println(otherConfigs);
        }
    }

    public static String getOtherConfigs() {
        return otherConfigs;
    }

    public static void setOtherConfigs(String otherConfigs) {
        ReadConfig.otherConfigs = otherConfigs;
    }

    public static HashMap<String, String> getOut() {
        return out;
    }
}
