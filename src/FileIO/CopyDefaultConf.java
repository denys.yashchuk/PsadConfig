package FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Copies file with clear default configs to the /etc/psad/psad.conf
 */
public class CopyDefaultConf {

    public static void copyDefaultConf() throws FileNotFoundException {
        File file = new File("default.conf");
        Scanner sc = new Scanner(file);
        Formatter f = new Formatter("/etc/psad/psad.conf");
        while (sc.hasNextLine()) {
            f.format("%s\n", sc.nextLine());
        }
        f.close();
    }

}
