package FileIO;

import java.io.FileNotFoundException;
import java.util.Formatter;

/**
 * Writes configs to the /etc/psad/psad.conf file
 */
public class SaveConfig {

    public static void save(String conf) throws FileNotFoundException {
        Formatter f = new Formatter("/etc/psad/psad.conf");
        f.format("%s\n\n", conf);
        f.format("%s", ReadConfig.getOtherConfigs().trim());
        f.close();
    }

}
