package GUI;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 * Creates Error alerts
 */
public class ErrorMessage extends Application {

    private static Alert alert;

    public void start(Stage primaryStage, int n) throws Exception {
        alert = new Alert(Alert.AlertType.ERROR);
        setErrorMessage(n);
        alert.setTitle("Error");
        start(primaryStage);
    }


    public void start(Stage primaryStage, String s) throws Exception {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(s);
        alert.setTitle("Warning");
        start(primaryStage);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        alert.setHeaderText(null);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();

    }

    private static void setErrorMessage(int n) {
        switch (n) {
            case 1:
                alert.setContentText("Wrong path to the psad.conf.");
                break;
        }
    }

}
