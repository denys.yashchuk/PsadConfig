package GUI;

import FileIO.ReadConfig;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Creates window for changing other configs
 */
public class OtherConfigs extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        BorderPane rootNode = new BorderPane();

        // Text Area for representing and changing configs
        TextArea configs = new TextArea();
        configs.setPrefSize(660, 720);
        configs.setText(ReadConfig.getOtherConfigs().trim());

        rootNode.setCenter(configs);

        // Button for saving changes
        Button saveButton = new Button("Save");
        saveButton.setMinHeight(40);
        saveButton.setOnAction((event) -> {
            ReadConfig.setOtherConfigs(configs.getText());
            try {
                new GUI().start(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        saveButton.setAlignment(Pos.CENTER);

        HBox saveButtonBox = new HBox();
        saveButtonBox.getChildren().add(saveButton);
        saveButtonBox.setPadding(new Insets(10));
        saveButtonBox.setAlignment(Pos.CENTER);

        rootNode.setBottom(saveButtonBox);

        Scene myScene = new Scene(rootNode, 700, 800); // creates scene with such size

        primaryStage.setScene(myScene); // sets scene to the stage
        primaryStage.setResizable(false); // block changing size
        primaryStage.sizeToScene(); // size of stage like size of scene
        primaryStage.setTitle("PSAD Configuration"); // set title
        primaryStage.show(); // shows stage
    }

}
