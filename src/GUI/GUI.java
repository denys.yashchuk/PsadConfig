package GUI;

import FileIO.ReadConfig;
import FileIO.SaveConfig;
import Logic.ErrorChecker;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.util.HashMap;

/**
 * Creates the window, where you can change configs value
 */
public class GUI extends Application {

    private Stage primaryStage;

    // Text Fields for config parameters
    private TextField emailAddressTextField = new TextField();
    private TextField hostnameTextField = new TextField();
    private TextField homeNetTextField = new TextField();
    private TextField externalNetTextField = new TextField();
    private TextField importOldScansTextField = new TextField();
    private TextField fwSearchAllTextField = new TextField();
    private TextField fwMsgSearchTextField = new TextField();
    private TextField syslogDaemonTextField = new TextField();
    private TextField ignorePortsTextField = new TextField();
    private TextField enablePersistenceTextField = new TextField();
    private TextField scanTimeoutTextField = new TextField();
    private TextField dangerLevel1TextField = new TextField();
    private TextField dangerLevel2TextField = new TextField();
    private TextField dangerLevel3TextField = new TextField();
    private TextField dangerLevel4TextField = new TextField();
    private TextField dangerLevel5TextField = new TextField();
    private TextField psadwatchCheckIntervalTextField = new TextField();
    private TextField portRangeScanThresholdTextField = new TextField();
    private TextField showAllSignaturesTextField = new TextField();
    private TextField snortSidStrTextField = new TextField();
    private TextField enableDshieldAlertsTextField = new TextField();
    private TextField dshieldAlertEmailTextField = new TextField();
    private TextField dshieldAlertIntervalTextField = new TextField();
    private TextField dshieldUserIDTextField = new TextField();
    private TextField dshieldUserEmailTextField = new TextField();
    private TextField dshieldDLThresholdTextField = new TextField();
    private TextField ignoreConntrackBugPktsTextField = new TextField();
    private TextField alertAllTextField = new TextField();
    private TextField emailLimitTextField = new TextField();
    private TextField emailAlertDangerLevelTextField = new TextField();
    private TextField enableAutoIDSTextField = new TextField();
    private TextField autoIDSDangerLevelTextField = new TextField();
    private TextField autoBlockTimeoutTextField = new TextField();
    private TextField iptablesBlockMethodsTextField = new TextField();
    private TextField tcpwrappersBlockMethodTextField = new TextField();
    private TextField whoisTimeoutTextField = new TextField();
    private TextField whoisLookupThresholdTextField = new TextField();
    private TextField dnsLookupThresholdTextField = new TextField();
    private TextField enableEXTScriptExecTextField = new TextField();
    private TextField externalScriptTextField = new TextField();
    private TextField execEXTScriptPerAlertTextField = new TextField();

    // Labels for config parameters
    private Label emailAddressLabel;
    private Label hostnameLabel;
    private Label homeNetLabel;
    private Label externalNetLabel;
    private Label importOldScansLabel;
    private Label fwSearchAllLabel;
    private Label fwMsgSearchLabel;
    private Label syslogDaemonLabel;
    private Label ignorePortsLabel;
    private Label enablePersistenceLabel;
    private Label scanTimeoutLabel;
    private Label dangerLevel1Label;
    private Label dangerLevel2Label;
    private Label dangerLevel3Label;
    private Label dangerLevel4Label;
    private Label dangerLevel5Label;
    private Label psadwatchCheckIntervalLabel;
    private Label portRangeScanThresholdLabel;
    private Label showAllSignaturesLabel;
    private Label snortSidStrLabel;
    private Label enableDshieldAlertsLabel;
    private Label dshieldAlertEmailLabel;
    private Label dshieldAlertIntervalLabel;
    private Label dshieldUserIDLabel;
    private Label dshieldUserEmailLabel;
    private Label dshieldDLThresholdLabel;
    private Label ignoreConntrackBugPktsLabel;
    private Label alertAllLabel;
    private Label emailLimitLabel;
    private Label emailAlertDangerLevelLabel;
    private Label enableAutoIDSLabel;
    private Label autoIDSDangerLevelLabel;
    private Label autoBlockTimeoutLabel;
    private Label iptablesBlockMethodsLabel;
    private Label tcpwrappersBlockMethodLabel;
    private Label whoisTimeoutLabel;
    private Label whoisLookupThresholdLabel;
    private Label dnsLookupThresholdLabel;
    private Label enableEXTScriptExecLabel;
    private Label externalScriptLabel;
    private Label execEXTScriptPerAlertLabel;

    Label[] labels = new Label[41];

    private TextField search;

    @Override
    public void start(Stage primaryStage) throws Exception {

        // Reads configs from /etc/psad/psad.conf file
        try {
            ReadConfig.readFile();
        } catch (FileNotFoundException e) {
            try {
                new ErrorMessage().start(primaryStage, 1);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        this.primaryStage = primaryStage;
        BorderPane rootNode = new BorderPane(); // creates border pane

        // Adds content to the root node
        createParametersBoxes(rootNode);

        BorderPane basicNode = new BorderPane(); // creates border pane

        ScrollPane sp = new ScrollPane(); // for scroll bar
        sp.setContent(rootNode); // add rootNode to ScrollPane
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // hide horizontal scrollBar
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS); // show only vertical scroll bar

        basicNode.setCenter(sp);

        // Button for saving configs
        Button saveButton = new Button("Save");
        saveButton.setMinHeight(40);
        saveButton.setOnAction((event) -> {
            try {
                // Checks input
                checkForInputExceptions();
                // Saving configs to the file
                SaveConfig.save(getConfigs());
                new StartMenu().start(primaryStage);
            } catch (Exception e) {
                try {
                    new ErrorMessage().start(primaryStage, e.getMessage());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        // Button for opening start window
        Button backButton = new Button("Back");
        backButton.setMinHeight(40);
        backButton.setOnAction((event) -> {
            try {
                new StartMenu().start(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // Config the other part of the file
        Button changeOtherConfig = new Button("Change other configs");
        changeOtherConfig.setMinHeight(40);
        changeOtherConfig.setOnAction((event) -> {
            try {
                new OtherConfigs().start(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // Horizontal Box with buttons
        HBox bottomButtons = new HBox(20);
        bottomButtons.setPadding(new Insets(15));
        bottomButtons.setAlignment(Pos.CENTER);
        bottomButtons.getChildren().addAll(changeOtherConfig, saveButton, backButton);

        basicNode.setBottom(bottomButtons);

        search = new TextField();
        search.setPromptText("Search...");
        search.setMinHeight(30);
        search.textProperty().addListener((observable, oldValue, newValue) -> {
            highlightLabels();
        });
        basicNode.setTop(search);

        Scene myScene = new Scene(basicNode, 700, 800); // creates scene with such size

        primaryStage.setScene(myScene); // sets scene to the stage
        primaryStage.setResizable(false); // block changing size
        primaryStage.sizeToScene(); // size of stage like size of scene
        primaryStage.setTitle("PSAD Configuration"); // set title
        primaryStage.show(); // shows stage

    }

    /**
     * Shows help message
     *
     * @param n - number of the help message
     * @throws Exception -
     */
    private void showHelp(int n) throws Exception {
        new HelpMessage().start(primaryStage, n);
    }

    /**
     * Creates Vertical Boxes with labels, help buttons,text fields and adds it to the root node
     *
     * @param rootNode
     */
    private void createParametersBoxes(BorderPane rootNode) {

        Image helpButtonImage = new Image(getClass().getResourceAsStream("help_button_image20.png"));

        //GUI elements for EMAIL_ADDRESSES config;
        emailAddressLabel = new Label("EMAIL_ADDRESSES");
        emailAddressLabel.setMinHeight(30);
        emailAddressLabel.setFont(new Font(15));
        labels[0] = emailAddressLabel;

        Button emailAddressButton = new Button();
        emailAddressButton.setGraphic(new ImageView(helpButtonImage)); // add image to button
        emailAddressButton.setOnAction((event -> {
            try {
                showHelp(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        emailAddressTextField.setPrefWidth(300);
        emailAddressTextField.setMinHeight(30);

        //GUI elements for HOSTNAME config;
        hostnameLabel = new Label("HOSTNAME");
        hostnameLabel.setMinHeight(30);
        hostnameLabel.setFont(new Font(15));
        labels[1] = hostnameLabel;

        Button hostnameButton = new Button();
        hostnameButton.setGraphic(new ImageView(helpButtonImage));
        hostnameButton.setOnAction((event -> {
            try {
                showHelp(2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        hostnameTextField.setPrefWidth(300);
        hostnameTextField.setMinHeight(30);

        //GUI elements for HOME_NET config;
        homeNetLabel = new Label("HOME_NET");
        homeNetLabel.setMinHeight(30);
        homeNetLabel.setFont(new Font(15));
        labels[2] = homeNetLabel;

        Button homeNetButton = new Button();
        homeNetButton.setGraphic(new ImageView(helpButtonImage));
        homeNetButton.setOnAction((event -> {
            try {
                showHelp(3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        homeNetTextField.setPrefWidth(300);
        homeNetTextField.setMinHeight(30);

        //GUI elements for EXTERNAL_NET config;
        externalNetLabel = new Label("EXTERNAL_NET");
        externalNetLabel.setMinHeight(30);
        externalNetLabel.setFont(new Font(15));
        labels[3] = externalNetLabel;

        Button externalNetButton = new Button();
        externalNetButton.setGraphic(new ImageView(helpButtonImage));
        externalNetButton.setOnAction((event -> {
            try {
                showHelp(4);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        externalNetTextField.setPrefWidth(300);
        externalNetTextField.setMinHeight(30);

        //GUI elements for IMPORT_OLD_SCANS config;
        importOldScansLabel = new Label("IMPORT_OLD_SCANS");
        importOldScansLabel.setMinHeight(30);
        importOldScansLabel.setFont(new Font(15));
        labels[4] = importOldScansLabel;

        Button importOldScansButton = new Button();
        importOldScansButton.setGraphic(new ImageView(helpButtonImage));
        importOldScansButton.setOnAction((event -> {
            try {
                showHelp(5);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Button
        importOldScansTextField.setPrefWidth(300);
        importOldScansTextField.setMinHeight(30);

        //GUI elements for FW_SEARCH_ALL config;
        fwSearchAllLabel = new Label("FW_SEARCH_ALL");
        fwSearchAllLabel.setMinHeight(30);
        fwSearchAllLabel.setFont(new Font(15));
        labels[5] = fwSearchAllLabel;

        Button fwSearchAllButton = new Button();
        fwSearchAllButton.setGraphic(new ImageView(helpButtonImage));
        fwSearchAllButton.setOnAction((event -> {
            try {
                showHelp(6);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Button
        fwSearchAllTextField.setPrefWidth(300);
        fwSearchAllTextField.setMinHeight(30);

        //GUI elements for FW_MSG_SEARCH config;
        fwMsgSearchLabel = new Label("FW_MSG_SEARCH");
        fwMsgSearchLabel.setMinHeight(30);
        fwMsgSearchLabel.setFont(new Font(15));
        labels[6] = fwMsgSearchLabel;

        Button fwMsgSearchButton = new Button();
        fwMsgSearchButton.setGraphic(new ImageView(helpButtonImage));
        fwMsgSearchButton.setOnAction((event -> {
            try {
                showHelp(7);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        fwMsgSearchTextField.setPrefWidth(300);
        fwMsgSearchTextField.setMinHeight(30);

        //GUI elements for SYSLOG_DAEMON config;
        syslogDaemonLabel = new Label("SYSLOG_DAEMON");
        syslogDaemonLabel.setMinHeight(30);
        syslogDaemonLabel.setFont(new Font(15));
        labels[7] = syslogDaemonLabel;

        Button syslogDaemonButton = new Button();
        syslogDaemonButton.setGraphic(new ImageView(helpButtonImage));
        syslogDaemonButton.setOnAction((event -> {
            try {
                showHelp(8);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        syslogDaemonTextField.setPrefWidth(300);
        syslogDaemonTextField.setMinHeight(30);

        //GUI elements for IGNORE_PORTS config;
        ignorePortsLabel = new Label("IGNORE_PORTS");
        ignorePortsLabel.setMinHeight(30);
        ignorePortsLabel.setFont(new Font(15));
        labels[8] = ignorePortsLabel;

        Button ignorePortsButton = new Button();
        ignorePortsButton.setGraphic(new ImageView(helpButtonImage));
        ignorePortsButton.setOnAction((event -> {
            try {
                showHelp(9);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        ignorePortsTextField.setPrefWidth(300);
        ignorePortsTextField.setMinHeight(30);

        //GUI elements for ENABLE_PERSISTENCE config;
        enablePersistenceLabel = new Label("ENABLE_PERSISTENCE");
        enablePersistenceLabel.setMinHeight(30);
        enablePersistenceLabel.setFont(new Font(15));
        labels[9] = enablePersistenceLabel;

        Button enablePersistenceButton = new Button();
        enablePersistenceButton.setGraphic(new ImageView(helpButtonImage));
        enablePersistenceButton.setOnAction((event -> {
            try {
                showHelp(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Button
        enablePersistenceTextField.setPrefWidth(300);
        enablePersistenceTextField.setMinHeight(30);

        //GUI elements for SCAN_TIMEOUT config;
        scanTimeoutLabel = new Label("SCAN_TIMEOUT");
        scanTimeoutLabel.setMinHeight(30);
        scanTimeoutLabel.setFont(new Font(15));
        labels[10] = scanTimeoutLabel;

        Button scanTimeoutButton = new Button();
        scanTimeoutButton.setGraphic(new ImageView(helpButtonImage));
        scanTimeoutButton.setOnAction((event -> {
            try {
                showHelp(11);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        scanTimeoutTextField.setPrefWidth(300);
        scanTimeoutTextField.setMinHeight(30);

        //GUI elements for DANGER_LEVEL1 config;
        dangerLevel1Label = new Label("DANGER_LEVEL1");
        dangerLevel1Label.setMinHeight(30);
        dangerLevel1Label.setFont(new Font(15));
        labels[11] = dangerLevel1Label;

        Button dangerLevel1Button = new Button();
        dangerLevel1Button.setGraphic(new ImageView(helpButtonImage));
        dangerLevel1Button.setOnAction((event -> {
            try {
                showHelp(12);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dangerLevel1TextField.setPrefWidth(300);
        dangerLevel1TextField.setMinHeight(30);

        //GUI elements for DANGER_LEVEL2 config;
        dangerLevel2Label = new Label("DANGER_LEVEL2");
        dangerLevel2Label.setMinHeight(30);
        dangerLevel2Label.setFont(new Font(15));
        labels[12] = dangerLevel2Label;

        Button dangerLevel2Button = new Button();
        dangerLevel2Button.setGraphic(new ImageView(helpButtonImage));
        dangerLevel2Button.setOnAction((event -> {
            try {
                showHelp(12);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dangerLevel2TextField.setPrefWidth(300);
        dangerLevel2TextField.setMinHeight(30);

        //GUI elements for DANGER_LEVEL3 config;
        dangerLevel3Label = new Label("DANGER_LEVEL3");
        dangerLevel3Label.setMinHeight(30);
        dangerLevel3Label.setFont(new Font(15));
        labels[13] = dangerLevel3Label;

        Button dangerLevel3Button = new Button();
        dangerLevel3Button.setGraphic(new ImageView(helpButtonImage));
        dangerLevel3Button.setOnAction((event -> {
            try {
                showHelp(12);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dangerLevel3TextField.setPrefWidth(300);
        dangerLevel3TextField.setMinHeight(30);

        //GUI elements for DANGER_LEVEL4 config;
        dangerLevel4Label = new Label("DANGER_LEVEL4");
        dangerLevel4Label.setMinHeight(30);
        dangerLevel4Label.setFont(new Font(15));
        labels[14] = dangerLevel4Label;

        Button dangerLevel4Button = new Button();
        dangerLevel4Button.setGraphic(new ImageView(helpButtonImage));
        dangerLevel4Button.setOnAction((event -> {
            try {
                showHelp(12);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dangerLevel4TextField.setPrefWidth(300);
        dangerLevel4TextField.setMinHeight(30);

        //GUI elements for DANGER_LEVEL5 config;
        dangerLevel5Label = new Label("DANGER_LEVEL5");
        dangerLevel5Label.setMinHeight(30);
        dangerLevel5Label.setFont(new Font(15));
        labels[15] = dangerLevel5Label;

        Button dangerLevel5Button = new Button();
        dangerLevel5Button.setGraphic(new ImageView(helpButtonImage));
        dangerLevel5Button.setOnAction((event -> {
            try {
                showHelp(12);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dangerLevel5TextField.setPrefWidth(300);
        dangerLevel5TextField.setMinHeight(30);

        //GUI elements for PSADWATCHD_CHECK_INTERVAL config;
        psadwatchCheckIntervalLabel = new Label("PSADWATCHD_CHECK_INTERVAL");
        psadwatchCheckIntervalLabel.setMinHeight(30);
        psadwatchCheckIntervalLabel.setFont(new Font(15));
        labels[16] = psadwatchCheckIntervalLabel;

        Button psadwatchCheckIntervalButton = new Button();
        psadwatchCheckIntervalButton.setGraphic(new ImageView(helpButtonImage));
        psadwatchCheckIntervalButton.setOnAction((event -> {
            try {
                showHelp(13);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        psadwatchCheckIntervalTextField.setPrefWidth(300);
        psadwatchCheckIntervalTextField.setMinHeight(30);

        //GUI elements for PORT_RANGE_SCAN_THRESHOLD config;
        portRangeScanThresholdLabel = new Label("PORT_RANGE_SCAN_THRESHOLD");
        portRangeScanThresholdLabel.setMinHeight(30);
        portRangeScanThresholdLabel.setFont(new Font(15));
        labels[17] = portRangeScanThresholdLabel;

        Button portRangeScanThresholdButton = new Button();
        portRangeScanThresholdButton.setGraphic(new ImageView(helpButtonImage));
        portRangeScanThresholdButton.setOnAction((event -> {
            try {
                showHelp(14);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        portRangeScanThresholdTextField.setPrefWidth(300);
        portRangeScanThresholdTextField.setMinHeight(30);

        //GUI elements for SHOW_ALL_SIGNATURES config;
        showAllSignaturesLabel = new Label("SHOW_ALL_SIGNATURES");
        showAllSignaturesLabel.setMinHeight(30);
        showAllSignaturesLabel.setFont(new Font(15));
        labels[18] = showAllSignaturesLabel;

        Button showAllSignaturesButton = new Button();
        showAllSignaturesButton.setGraphic(new ImageView(helpButtonImage));
        showAllSignaturesButton.setOnAction((event -> {
            try {
                showHelp(15);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Button
        showAllSignaturesTextField.setPrefWidth(300);
        showAllSignaturesTextField.setMinHeight(30);

        //GUI elements for SNORT_SID_STR config;
        snortSidStrLabel = new Label("SNORT_SID_STR");
        snortSidStrLabel.setMinHeight(30);
        snortSidStrLabel.setFont(new Font(15));
        labels[19] = snortSidStrLabel;

        Button snortSidStrButton = new Button();
        snortSidStrButton.setGraphic(new ImageView(helpButtonImage));
        snortSidStrButton.setOnAction((event -> {
            try {
                showHelp(16);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        snortSidStrTextField.setPrefWidth(300);
        snortSidStrTextField.setMinHeight(30);

        //GUI elements for ENABLE_DSHIELD_ALERTS config;
        enableDshieldAlertsLabel = new Label("ENABLE_DSHIELD_ALERTS");
        enableDshieldAlertsLabel.setMinHeight(30);
        enableDshieldAlertsLabel.setFont(new Font(15));
        labels[20] = enableDshieldAlertsLabel;

        Button enableDshieldAlertsButton = new Button();
        enableDshieldAlertsButton.setGraphic(new ImageView(helpButtonImage));
        enableDshieldAlertsButton.setOnAction((event -> {
            try {
                showHelp(17);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Rudio Button
        enableDshieldAlertsTextField.setPrefWidth(300);
        enableDshieldAlertsTextField.setMinHeight(30);

        //GUI elements for DSHIELD_ALERT_EMAIL config;
        dshieldAlertEmailLabel = new Label("DSHIELD_ALERT_EMAIL");
        dshieldAlertEmailLabel.setMinHeight(30);
        dshieldAlertEmailLabel.setFont(new Font(15));
        labels[21] = dshieldAlertEmailLabel;

        Button dshieldAlertEmailButton = new Button();
        dshieldAlertEmailButton.setGraphic(new ImageView(helpButtonImage));
        dshieldAlertEmailButton.setOnAction((event -> {
            try {
                showHelp(18);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        dshieldAlertEmailTextField.setPrefWidth(300);
        dshieldAlertEmailTextField.setMinHeight(30);

        //GUI elements for DSHIELD_ALERT_INTERVAL config;
        dshieldAlertIntervalLabel = new Label("DSHIELD_ALERT_INTERVAL");
        dshieldAlertIntervalLabel.setMinHeight(30);
        dshieldAlertIntervalLabel.setFont(new Font(15));
        labels[22] = dshieldAlertIntervalLabel;

        Button dshieldAlertIntervalButton = new Button();
        dshieldAlertIntervalButton.setGraphic(new ImageView(helpButtonImage));
        dshieldAlertIntervalButton.setOnAction((event -> {
            try {
                showHelp(19);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dshieldAlertIntervalTextField.setPrefWidth(300);
        dshieldAlertIntervalTextField.setMinHeight(30);

        //GUI elements for DSHIELD_USER_ID config;
        dshieldUserIDLabel = new Label("DSHIELD_USER_ID");
        dshieldUserIDLabel.setMinHeight(30);
        dshieldUserIDLabel.setFont(new Font(15));
        labels[23] = dshieldUserIDLabel;

        Button dshieldUserIDButton = new Button();
        dshieldUserIDButton.setGraphic(new ImageView(helpButtonImage));
        dshieldUserIDButton.setOnAction((event -> {
            try {
                showHelp(20);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dshieldUserIDTextField.setPrefWidth(300);
        dshieldUserIDTextField.setMinHeight(30);

        //GUI elements for DSHIELD_USER_EMAIL config;
        dshieldUserEmailLabel = new Label("DSHIELD_USER_EMAIL");
        dshieldUserEmailLabel.setMinHeight(30);
        dshieldUserEmailLabel.setFont(new Font(15));
        labels[24] = dshieldUserEmailLabel;

        Button dshieldUserEmailButton = new Button();
        dshieldUserEmailButton.setGraphic(new ImageView(helpButtonImage));
        dshieldUserEmailButton.setOnAction((event -> {
            try {
                showHelp(21);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        dshieldUserEmailTextField.setPrefWidth(300);
        dshieldUserEmailTextField.setMinHeight(30);

        //GUI elements for DSHIELD_DL_THRESHOLD config;
        dshieldDLThresholdLabel = new Label("DSHIELD_DL_THRESHOLD");
        dshieldDLThresholdLabel.setMinHeight(30);
        dshieldDLThresholdLabel.setFont(new Font(15));
        labels[25] = dshieldDLThresholdLabel;

        Button dshieldDLThresholdButton = new Button();
        dshieldDLThresholdButton.setGraphic(new ImageView(helpButtonImage));
        dshieldDLThresholdButton.setOnAction((event -> {
            try {
                showHelp(22);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dshieldDLThresholdTextField.setPrefWidth(300);
        dshieldDLThresholdTextField.setMinHeight(30);

        //GUI elements for IGNORE_CONNTRACK_BUG_PKTS config;
        ignoreConntrackBugPktsLabel = new Label("IGNORE_CONNTRACK_BUG_PKTS");
        ignoreConntrackBugPktsLabel.setMinHeight(30);
        ignoreConntrackBugPktsLabel.setFont(new Font(15));
        labels[26] = ignoreConntrackBugPktsLabel;

        Button ignoreConntrackBugPktsButton = new Button();
        ignoreConntrackBugPktsButton.setGraphic(new ImageView(helpButtonImage));
        ignoreConntrackBugPktsButton.setOnAction((event -> {
            try {
                showHelp(23);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        ignoreConntrackBugPktsTextField.setPrefWidth(300);
        ignoreConntrackBugPktsTextField.setMinHeight(30);

        //GUI elements for ALERT_ALL config;
        alertAllLabel = new Label("ALERT_ALL");
        alertAllLabel.setMinHeight(30);
        alertAllLabel.setFont(new Font(15));
        labels[27] = alertAllLabel;

        Button alertAllButton = new Button();
        alertAllButton.setGraphic(new ImageView(helpButtonImage));
        alertAllButton.setOnAction((event -> {
            try {
                showHelp(24);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        alertAllTextField.setPrefWidth(300);
        alertAllTextField.setMinHeight(30);

        //GUI elements for EMAIL_LIMIT config;
        emailLimitLabel = new Label("EMAIL_LIMIT");
        emailLimitLabel.setMinHeight(30);
        emailLimitLabel.setFont(new Font(15));
        labels[28] = emailLimitLabel;

        Button emailLimitButton = new Button();
        emailLimitButton.setGraphic(new ImageView(helpButtonImage));
        emailLimitButton.setOnAction((event -> {
            try {
                showHelp(25);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        emailLimitTextField.setPrefWidth(300);
        emailLimitTextField.setMinHeight(30);

        //GUI elements for EMAIL_ALERT_DANGER_LEVEL config;
        emailAlertDangerLevelLabel = new Label("EMAIL_ALERT_DANGER_LEVEL");
        emailAlertDangerLevelLabel.setMinHeight(30);
        emailAlertDangerLevelLabel.setFont(new Font(15));
        labels[29] = emailAlertDangerLevelLabel;

        Button emailAlertDangerLevelButton = new Button();
        emailAlertDangerLevelButton.setGraphic(new ImageView(helpButtonImage));
        emailAlertDangerLevelButton.setOnAction((event -> {
            try {
                showHelp(26);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        emailAlertDangerLevelTextField.setPrefWidth(300);
        emailAlertDangerLevelTextField.setMinHeight(30);

        //GUI elements for ENABLE_AUTO_IDS config;
        enableAutoIDSLabel = new Label("ENABLE_AUTO_IDS");
        enableAutoIDSLabel.setMinHeight(30);
        enableAutoIDSLabel.setFont(new Font(15));
        labels[30] = enableAutoIDSLabel;

        Button enableAutoIDSButton = new Button();
        enableAutoIDSButton.setGraphic(new ImageView(helpButtonImage));
        enableAutoIDSButton.setOnAction((event -> {
            try {
                showHelp(27);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        enableAutoIDSTextField.setPrefWidth(300);
        enableAutoIDSTextField.setMinHeight(30);

        //GUI elements for AUTO_IDS_DANGER_LEVEL config;
        autoIDSDangerLevelLabel = new Label("AUTO_IDS_DANGER_LEVEL");
        autoIDSDangerLevelLabel.setMinHeight(30);
        autoIDSDangerLevelLabel.setFont(new Font(15));
        labels[31] = autoIDSDangerLevelLabel;

        Button autoIDSDangerLevelButton = new Button();
        autoIDSDangerLevelButton.setGraphic(new ImageView(helpButtonImage));
        autoIDSDangerLevelButton.setOnAction((event -> {
            try {
                showHelp(28);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        autoIDSDangerLevelTextField.setPrefWidth(300);
        autoIDSDangerLevelTextField.setMinHeight(30);

        //GUI elements for AUTO_BLOCK_TIMEOUT config;
        autoBlockTimeoutLabel = new Label("AUTO_BLOCK_TIMEOUT");
        autoBlockTimeoutLabel.setMinHeight(30);
        autoBlockTimeoutLabel.setFont(new Font(15));
        labels[32] = autoBlockTimeoutLabel;

        Button autoBlockTimeoutButton = new Button();
        autoBlockTimeoutButton.setGraphic(new ImageView(helpButtonImage));
        autoBlockTimeoutButton.setOnAction((event -> {
            try {
                showHelp(29);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        autoBlockTimeoutTextField.setPrefWidth(300);
        autoBlockTimeoutTextField.setMinHeight(30);

        //GUI elements for IPTABLES_BLOCK_METHOD config;
        iptablesBlockMethodsLabel = new Label("IPTABLES_BLOCK_METHOD");
        iptablesBlockMethodsLabel.setMinHeight(30);
        iptablesBlockMethodsLabel.setFont(new Font(15));
        labels[33] = iptablesBlockMethodsLabel;

        Button iptablesBlockMethodsButton = new Button();
        iptablesBlockMethodsButton.setGraphic(new ImageView(helpButtonImage));
        iptablesBlockMethodsButton.setOnAction((event -> {
            try {
                showHelp(30);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        iptablesBlockMethodsTextField.setPrefWidth(300);
        iptablesBlockMethodsTextField.setMinHeight(30);

        //GUI elements for TCPWRAPPERS_BLOCK_METHOD config;
        tcpwrappersBlockMethodLabel = new Label("TCPWRAPPERS_BLOCK_METHOD");
        tcpwrappersBlockMethodLabel.setMinHeight(30);
        tcpwrappersBlockMethodLabel.setFont(new Font(15));
        labels[34] = tcpwrappersBlockMethodLabel;

        Button tcpwrappersBlockMethodButton = new Button();
        tcpwrappersBlockMethodButton.setGraphic(new ImageView(helpButtonImage));
        tcpwrappersBlockMethodButton.setOnAction((event -> {
            try {
                showHelp(31);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        tcpwrappersBlockMethodTextField.setPrefWidth(300);
        tcpwrappersBlockMethodTextField.setMinHeight(30);

        //GUI elements for WHOIS_TIMEOUT config;
        whoisTimeoutLabel = new Label("WHOIS_TIMEOUT");
        whoisTimeoutLabel.setMinHeight(30);
        whoisTimeoutLabel.setFont(new Font(15));
        labels[35] = whoisTimeoutLabel;

        Button whoisTimeoutButton = new Button();
        whoisTimeoutButton.setGraphic(new ImageView(helpButtonImage));
        whoisTimeoutButton.setOnAction((event -> {
            try {
                showHelp(32);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        whoisTimeoutTextField.setPrefWidth(300);
        whoisTimeoutTextField.setMinHeight(30);

        //GUI elements for WHOIS_LOOKUP_THRESHOLD config;
        whoisLookupThresholdLabel = new Label("WHOIS_LOOKUP_THRESHOLD");
        whoisLookupThresholdLabel.setMinHeight(30);
        whoisLookupThresholdLabel.setFont(new Font(15));
        labels[36] = whoisLookupThresholdLabel;

        Button whoisLookupThresholdButton = new Button();
        whoisLookupThresholdButton.setGraphic(new ImageView(helpButtonImage));
        whoisLookupThresholdButton.setOnAction((event -> {
            try {
                showHelp(33);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        whoisLookupThresholdTextField.setPrefWidth(300);
        whoisLookupThresholdTextField.setMinHeight(30);

        //GUI elements for DNS_LOOKUP_THRESHOLD config;
        dnsLookupThresholdLabel = new Label("DNS_LOOKUP_THRESHOLD");
        dnsLookupThresholdLabel.setMinHeight(30);
        dnsLookupThresholdLabel.setFont(new Font(15));
        labels[37] = dnsLookupThresholdLabel;

        Button dnsLookupThresholdButton = new Button();
        dnsLookupThresholdButton.setGraphic(new ImageView(helpButtonImage));
        dnsLookupThresholdButton.setOnAction((event -> {
            try {
                showHelp(34);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Numbers
        dnsLookupThresholdTextField.setPrefWidth(300);
        dnsLookupThresholdTextField.setMinHeight(30);

        //GUI elements for ENABLE_EXT_SCRIPT_EXEC config;
        enableEXTScriptExecLabel = new Label("ENABLE_EXT_SCRIPT_EXEC");
        enableEXTScriptExecLabel.setMinHeight(30);
        enableEXTScriptExecLabel.setFont(new Font(15));
        labels[38] = enableEXTScriptExecLabel;

        Button enableEXTScriptExecButton = new Button();
        enableEXTScriptExecButton.setGraphic(new ImageView(helpButtonImage));
        enableEXTScriptExecButton.setOnAction((event -> {
            try {
                showHelp(35);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        enableEXTScriptExecTextField.setPrefWidth(300);
        enableEXTScriptExecTextField.setMinHeight(30);

        //GUI elements for EXTERNAL_SCRIPT config;
        externalScriptLabel = new Label("EXTERNAL_SCRIPT");
        externalScriptLabel.setMinHeight(30);
        externalScriptLabel.setFont(new Font(15));
        labels[39] = externalScriptLabel;

        Button externalScriptButton = new Button();
        externalScriptButton.setGraphic(new ImageView(helpButtonImage));
        externalScriptButton.setOnAction((event -> {
            try {
                showHelp(36);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        externalScriptTextField.setPrefWidth(300);
        externalScriptTextField.setMinHeight(30);

        //GUI elements for EXEC_EXT_SCRIPT_PER_ALERT config;
        execEXTScriptPerAlertLabel = new Label("EXEC_EXT_SCRIPT_PER_ALERT");
        execEXTScriptPerAlertLabel.setMinHeight(30);
        execEXTScriptPerAlertLabel.setFont(new Font(15));
        labels[40] = execEXTScriptPerAlertLabel;

        Button execEXTScriptPerAlertButton = new Button();
        execEXTScriptPerAlertButton.setGraphic(new ImageView(helpButtonImage));
        execEXTScriptPerAlertButton.setOnAction((event -> {
            try {
                showHelp(37);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
        //TODO: Radio Buttons
        execEXTScriptPerAlertTextField.setPrefWidth(300);
        execEXTScriptPerAlertTextField.setMinHeight(30);

        //Vertical box for all lables
        VBox labels = new VBox(10);
        labels.setPadding(new Insets(10));
        labels.getChildren().addAll(emailAddressLabel, hostnameLabel, homeNetLabel, externalNetLabel,
                importOldScansLabel, fwSearchAllLabel, fwMsgSearchLabel, syslogDaemonLabel, ignorePortsLabel,
                enablePersistenceLabel, scanTimeoutLabel, dangerLevel1Label, dangerLevel2Label, dangerLevel3Label,
                dangerLevel4Label, dangerLevel5Label, psadwatchCheckIntervalLabel, portRangeScanThresholdLabel,
                showAllSignaturesLabel, snortSidStrLabel, enableDshieldAlertsLabel, dshieldAlertEmailLabel,
                dshieldAlertIntervalLabel, dshieldUserIDLabel, dshieldUserEmailLabel, dshieldDLThresholdLabel,
                ignoreConntrackBugPktsLabel, alertAllLabel, emailLimitLabel, emailAlertDangerLevelLabel,
                enableAutoIDSLabel, autoIDSDangerLevelLabel, autoBlockTimeoutLabel, iptablesBlockMethodsLabel,
                tcpwrappersBlockMethodLabel, whoisTimeoutLabel, whoisLookupThresholdLabel, dnsLookupThresholdLabel,
                enableEXTScriptExecLabel, externalScriptLabel, execEXTScriptPerAlertLabel);

        //Vertical box for all buttons;
        VBox buttons = new VBox(10);
        buttons.setPadding(new Insets(10));
        buttons.getChildren().addAll(emailAddressButton, hostnameButton, homeNetButton, externalNetButton,
                importOldScansButton, fwSearchAllButton, fwMsgSearchButton, syslogDaemonButton, ignorePortsButton,
                enablePersistenceButton, scanTimeoutButton, dangerLevel1Button, dangerLevel2Button, dangerLevel3Button,
                dangerLevel4Button, dangerLevel5Button, psadwatchCheckIntervalButton, portRangeScanThresholdButton,
                showAllSignaturesButton, snortSidStrButton, enableDshieldAlertsButton, dshieldAlertEmailButton,
                dshieldAlertIntervalButton, dshieldUserIDButton, dshieldUserEmailButton, dshieldDLThresholdButton,
                ignoreConntrackBugPktsButton, alertAllButton, emailLimitButton, emailAlertDangerLevelButton,
                enableAutoIDSButton, autoIDSDangerLevelButton, autoBlockTimeoutButton, iptablesBlockMethodsButton,
                tcpwrappersBlockMethodButton, whoisTimeoutButton, whoisLookupThresholdButton, dnsLookupThresholdButton,
                enableEXTScriptExecButton, externalScriptButton, execEXTScriptPerAlertButton);

        //Vertical box for all changing fields;
        VBox textFields = new VBox(10);
        textFields.setPadding(new Insets(10));
        textFields.getChildren().addAll(emailAddressTextField, hostnameTextField, homeNetTextField, externalNetTextField,
                importOldScansTextField, fwSearchAllTextField, fwMsgSearchTextField, syslogDaemonTextField, ignorePortsTextField,
                enablePersistenceTextField, scanTimeoutTextField, dangerLevel1TextField, dangerLevel2TextField, dangerLevel3TextField,
                dangerLevel4TextField, dangerLevel5TextField, psadwatchCheckIntervalTextField, portRangeScanThresholdTextField,
                showAllSignaturesTextField, snortSidStrTextField, enableDshieldAlertsTextField, dshieldAlertEmailTextField,
                dshieldAlertIntervalTextField, dshieldUserIDTextField, dshieldUserEmailTextField, dshieldDLThresholdTextField,
                ignoreConntrackBugPktsTextField, alertAllTextField, emailLimitTextField, emailAlertDangerLevelTextField,
                enableAutoIDSTextField, autoIDSDangerLevelTextField, autoBlockTimeoutTextField, iptablesBlockMethodsTextField,
                tcpwrappersBlockMethodTextField, whoisTimeoutTextField, whoisLookupThresholdTextField, dnsLookupThresholdTextField,
                enableEXTScriptExecTextField, externalScriptTextField, execEXTScriptPerAlertTextField);
        setTextToTextFields();

        //Adding VBoxes to root node;
        rootNode.setLeft(labels);
        rootNode.setCenter(buttons);
        rootNode.setRight(textFields);
    }

    /**
     * Adds value of the config parameter to the appropriate text field
     */
    private void setTextToTextFields() {
        HashMap<String, String> conf = ReadConfig.getOut();
        emailAddressTextField.setText(conf.get("EMAIL_ADDRESSES"));
        hostnameTextField.setText(conf.get("HOSTNAME"));
        homeNetTextField.setText(conf.get("HOME_NET"));
        externalNetTextField.setText(conf.get("EXTERNAL_NET"));
        importOldScansTextField.setText(conf.get("IMPORT_OLD_SCANS"));
        fwSearchAllTextField.setText(conf.get("FW_SEARCH_ALL"));
        fwMsgSearchTextField.setText(conf.get("FW_MSG_SEARCH"));
        syslogDaemonTextField.setText(conf.get("SYSLOG_DAEMON"));
        ignorePortsTextField.setText(conf.get("IGNORE_PORTS"));
        enablePersistenceTextField.setText(conf.get("ENABLE_PERSISTENCE"));
        scanTimeoutTextField.setText(conf.get("SCAN_TIMEOUT"));
        dangerLevel1TextField.setText(conf.get("DANGER_LEVEL1"));
        dangerLevel2TextField.setText(conf.get("DANGER_LEVEL2"));
        dangerLevel3TextField.setText(conf.get("DANGER_LEVEL3"));
        dangerLevel4TextField.setText(conf.get("DANGER_LEVEL4"));
        dangerLevel5TextField.setText(conf.get("DANGER_LEVEL5"));
        psadwatchCheckIntervalTextField.setText(conf.get("PSADWATCHD_CHECK_INTERVAL"));
        portRangeScanThresholdTextField.setText(conf.get("PORT_RANGE_SCAN_THRESHOLD"));
        showAllSignaturesTextField.setText(conf.get("SHOW_ALL_SIGNATURES"));
        snortSidStrTextField.setText(conf.get("SNORT_SID_STR"));
        enableDshieldAlertsTextField.setText(conf.get("ENABLE_DSHIELD_ALERTS"));
        dshieldAlertEmailTextField.setText(conf.get("DSHIELD_ALERT_EMAIL"));
        dshieldAlertIntervalTextField.setText(conf.get("DSHIELD_ALERT_INTERVAL"));
        dshieldUserIDTextField.setText(conf.get("DSHIELD_USER_ID"));
        dshieldUserEmailTextField.setText(conf.get("DSHIELD_USER_EMAIL"));
        dshieldDLThresholdTextField.setText(conf.get("DSHIELD_DL_THRESHOLD"));
        ignoreConntrackBugPktsTextField.setText(conf.get("IGNORE_CONNTRACK_BUG_PKTS"));
        alertAllTextField.setText(conf.get("ALERT_ALL"));
        emailLimitTextField.setText(conf.get("EMAIL_LIMIT"));
        emailAlertDangerLevelTextField.setText(conf.get("EMAIL_ALERT_DANGER_LEVEL"));
        enableAutoIDSTextField.setText(conf.get("ENABLE_AUTO_IDS"));
        autoIDSDangerLevelTextField.setText(conf.get("AUTO_IDS_DANGER_LEVEL"));
        autoBlockTimeoutTextField.setText(conf.get("AUTO_BLOCK_TIMEOUT"));
        iptablesBlockMethodsTextField.setText(conf.get("IPTABLES_BLOCK_METHOD"));
        tcpwrappersBlockMethodTextField.setText(conf.get("TCPWRAPPERS_BLOCK_METHOD"));
        whoisTimeoutTextField.setText(conf.get("WHOIS_TIMEOUT"));
        whoisLookupThresholdTextField.setText(conf.get("WHOIS_LOOKUP_THRESHOLD"));
        dnsLookupThresholdTextField.setText(conf.get("DNS_LOOKUP_THRESHOLD"));
        enableEXTScriptExecTextField.setText(conf.get("ENABLE_EXT_SCRIPT_EXEC"));
        externalScriptTextField.setText(conf.get("EXTERNAL_SCRIPT"));
        execEXTScriptPerAlertTextField.setText(conf.get("EXEC_EXT_SCRIPT_PER_ALERT"));
    }

    /**
     * Checks if the config parameter value is correct
     *
     * @throws IllegalArgumentException
     */
    private void checkForInputExceptions() throws IllegalArgumentException {
        if (!ErrorChecker.yesNoChecker(importOldScansTextField.getText()))
            throw new IllegalArgumentException("Variable in the IMPORT_OLD_SCANS field must be \"Y\" or \"N\"");
        if (!ErrorChecker.yesNoChecker(fwSearchAllTextField.getText()))
            throw new IllegalArgumentException("Variable in the FW_SEARCH_ALL field must be \"Y\" or \"N\"");
        if (!ErrorChecker.yesNoChecker(enablePersistenceTextField.getText()))
            throw new IllegalArgumentException("Variable in the ENABLE_PERSISTENCE field must be \"Y\" or \"N\"");
        if (!ErrorChecker.numberChecker(scanTimeoutTextField.getText()))
            throw new IllegalArgumentException("Variable in the SCAN_TIMEOUT field must be numeric");
        if (!ErrorChecker.numberChecker(dangerLevel1TextField.getText()))
            throw new IllegalArgumentException("Variable in the DANGER_LEVEL1 field must be numeric");
        if (!ErrorChecker.numberChecker(dangerLevel2TextField.getText()))
            throw new IllegalArgumentException("Variable in the DANGER_LEVEL2 field must be numeric");
        if (!ErrorChecker.numberChecker(dangerLevel3TextField.getText()))
            throw new IllegalArgumentException("Variable in the DANGER_LEVEL3 field must be numeric");
        if (!ErrorChecker.numberChecker(dangerLevel4TextField.getText()))
            throw new IllegalArgumentException("Variable in the DANGER_LEVEL4 field must be numeric");
        if (!ErrorChecker.numberChecker(dangerLevel5TextField.getText()))
            throw new IllegalArgumentException("Variable in the DANGER_LEVEL5 field must be numeric");
        if (!ErrorChecker.numberChecker(psadwatchCheckIntervalTextField.getText()))
            throw new IllegalArgumentException("Variable in the PSADWATCHD_CHECK_INTERVAL field must be numeric");
        if (!ErrorChecker.numberChecker(portRangeScanThresholdTextField.getText()))
            throw new IllegalArgumentException("Variable in the PORT_RANGE_SCAN_THRESHOLD field must be numeric");
        if (!ErrorChecker.yesNoChecker(showAllSignaturesTextField.getText()))
            throw new IllegalArgumentException("Variable in the SHOW_ALL_SIGNATURES field must be \"Y\" or \"N\"");
        if (!ErrorChecker.yesNoChecker(enableDshieldAlertsTextField.getText()))
            throw new IllegalArgumentException("Variable in the ENABLE_DSHIELD_ALERTS field must be \"Y\" or \"N\"");
        if (!ErrorChecker.numberChecker(dshieldAlertIntervalTextField.getText()))
            throw new IllegalArgumentException("Variable in the DSHIELD_ALERT_INTERVAL field must be numeric");
        if (!ErrorChecker.numberChecker(dshieldUserIDTextField.getText()))
            throw new IllegalArgumentException("Variable in the DSHIELD_USER_ID field must be numeric");
        if (!ErrorChecker.numberChecker(dshieldDLThresholdTextField.getText()))
            throw new IllegalArgumentException("Variable in the DSHIELD_DL_THRESHOLD field must be numeric and less then 5 (including)");
        if (!ErrorChecker.yesNoChecker(ignoreConntrackBugPktsTextField.getText()))
            throw new IllegalArgumentException("Variable in the IGNORE_CONNTRACK_BUG_PKTS field must be \"Y\" or \"N\"");
        if (!ErrorChecker.yesNoChecker(alertAllTextField.getText()))
            throw new IllegalArgumentException("Variable in the ALERT_ALL field must be \"Y\" or \"N\"");
        if (!ErrorChecker.numberChecker(emailLimitTextField.getText()))
            throw new IllegalArgumentException("Variable in the EMAIL_LIMIT field must be numeric");
        if (!ErrorChecker.numberChecker(emailAlertDangerLevelTextField.getText()))
            throw new IllegalArgumentException("Variable in the EMAIL_ALERT_DANGER_LEVEL field must be numeric and less then 5 (including)");
        if (!ErrorChecker.yesNoChecker(enableAutoIDSTextField.getText()))
            throw new IllegalArgumentException("Variable in the ENABLE_AUTO_IDS field must be \"Y\" or \"N\"");
        if (!ErrorChecker.numberChecker(autoIDSDangerLevelTextField.getText()))
            throw new IllegalArgumentException("Variable in the AUTO_IDS_DANGER_LEVEL field must be numeric and less then 5 (including)");
        if (!ErrorChecker.numberChecker(autoBlockTimeoutTextField.getText()))
            throw new IllegalArgumentException("Variable in the AUTO_BLOCK_TIMEOUT field must be numeric");
        if (!ErrorChecker.yesNoChecker(iptablesBlockMethodsTextField.getText()))
            throw new IllegalArgumentException("Variable in the IPTABLES_BLOCK_METHOD field must be \"Y\" or \"N\"");
        if (!ErrorChecker.yesNoChecker(tcpwrappersBlockMethodTextField.getText()))
            throw new IllegalArgumentException("Variable in the TCPWRAPPERS_BLOCK_METHOD field must be \"Y\" or \"N\"");
        if (!ErrorChecker.numberChecker(whoisTimeoutTextField.getText()))
            throw new IllegalArgumentException("Variable in the WHOIS_TIMEOUT field must be numeric");
        if (!ErrorChecker.numberChecker(whoisLookupThresholdTextField.getText()))
            throw new IllegalArgumentException("Variable in the WHOIS_LOOKUP_THRESHOLD field must be numeric");
        if (!ErrorChecker.numberChecker(dnsLookupThresholdTextField.getText()))
            throw new IllegalArgumentException("Variable in the DNS_LOOKUP_THRESHOLD field must be numeric");
        if (!ErrorChecker.yesNoChecker(enableEXTScriptExecTextField.getText()))
            throw new IllegalArgumentException("Variable in the ENABLE_EXT_SCRIPT_EXEC field must be \"Y\" or \"N\"");
        if (!ErrorChecker.yesNoChecker(execEXTScriptPerAlertTextField.getText()))
            throw new IllegalArgumentException("Variable in the EXEC_EXT_SCRIPT_PER_ALERT field must be \"Y\" or \"N\"");
    }

    /**
     * Set all values of configs to the String
     *
     * @return - String with all values of configs to the String
     */
    private String getConfigs() {
        String out = "";
        out += "EMAIL_ADDRESSES\t" + emailAddressTextField.getText() + ";\n";
        out += "HOSTNAME\t" + hostnameTextField.getText() + ";\n";
        out += "HOME_NET\t" + homeNetTextField.getText() + ";\n";
        out += "EXTERNAL_NET\t" + externalNetTextField.getText() + ";\n";
        out += "IMPORT_OLD_SCANS\t" + importOldScansTextField.getText() + ";\n";
        out += "FW_SEARCH_ALL\t" + fwSearchAllTextField.getText() + ";\n";
        out += "FW_MSG_SEARCH\t" + fwMsgSearchTextField.getText() + ";\n";
        out += "SYSLOG_DAEMON\t" + syslogDaemonTextField.getText() + ";\n";
        out += "IGNORE_PORTS\t" + ignorePortsTextField.getText() + ";\n";
        out += "ENABLE_PERSISTENCE\t" + enablePersistenceTextField.getText() + ";\n";
        out += "SCAN_TIMEOUT\t" + scanTimeoutTextField.getText() + ";\n";
        out += "DANGER_LEVEL1\t" + dangerLevel1TextField.getText() + ";\n";
        out += "DANGER_LEVEL2\t" + dangerLevel2TextField.getText() + ";\n";
        out += "DANGER_LEVEL3\t" + dangerLevel3TextField.getText() + ";\n";
        out += "DANGER_LEVEL4\t" + dangerLevel4TextField.getText() + ";\n";
        out += "DANGER_LEVEL5\t" + dangerLevel5TextField.getText() + ";\n";
        out += "PSADWATCHD_CHECK_INTERVAL\t" + psadwatchCheckIntervalTextField.getText() + ";\n";
        out += "PORT_RANGE_SCAN_THRESHOLD\t" + portRangeScanThresholdTextField.getText() + ";\n";
        out += "SHOW_ALL_SIGNATURES\t" + showAllSignaturesTextField.getText() + ";\n";
        out += "SNORT_SID_STR\t" + snortSidStrTextField.getText() + ";\n";
        out += "ENABLE_DSHIELD_ALERTS\t" + enableDshieldAlertsTextField.getText() + ";\n";
        out += "DSHIELD_ALERT_EMAIL\t" + dshieldAlertEmailTextField.getText() + ";\n";
        out += "DSHIELD_ALERT_INTERVAL\t" + dshieldAlertIntervalTextField.getText() + ";\n";
        out += "DSHIELD_USER_ID\t" + dshieldUserIDTextField.getText() + ";\n";
        out += "DSHIELD_USER_EMAIL\t" + dshieldUserEmailTextField.getText() + ";\n";
        out += "DSHIELD_DL_THRESHOLD\t" + dshieldDLThresholdTextField.getText() + ";\n";
        out += "IGNORE_CONNTRACK_BUG_PKTS\t" + ignoreConntrackBugPktsTextField.getText() + ";\n";
        out += "ALERT_ALL\t" + alertAllTextField.getText() + ";\n";
        out += "EMAIL_LIMIT\t" + emailLimitTextField.getText() + ";\n";
        out += "EMAIL_ALERT_DANGER_LEVEL\t" + emailAlertDangerLevelTextField.getText() + ";\n";
        out += "ENABLE_AUTO_IDS\t" + enableAutoIDSTextField.getText() + ";\n";
        out += "AUTO_IDS_DANGER_LEVEL\t" + autoIDSDangerLevelTextField.getText() + ";\n";
        out += "AUTO_BLOCK_TIMEOUT\t" + autoBlockTimeoutTextField.getText() + ";\n";
        out += "IPTABLES_BLOCK_METHOD\t" + iptablesBlockMethodsTextField.getText() + ";\n";
        out += "TCPWRAPPERS_BLOCK_METHOD\t" + tcpwrappersBlockMethodTextField.getText() + ";\n";
        out += "WHOIS_TIMEOUT\t" + whoisTimeoutTextField.getText() + ";\n";
        out += "WHOIS_LOOKUP_THRESHOLD\t" + whoisLookupThresholdTextField.getText() + ";\n";
        out += "DNS_LOOKUP_THRESHOLD\t" + dnsLookupThresholdTextField.getText() + ";\n";
        out += "ENABLE_EXT_SCRIPT_EXEC\t" + enableEXTScriptExecTextField.getText() + ";\n";
        out += "EXTERNAL_SCRIPT\t" + externalScriptTextField.getText() + ";\n";
        out += "EXEC_EXT_SCRIPT_PER_ALERT\t" + execEXTScriptPerAlertTextField.getText() + ";\n";
        return out;
    }

    private void highlightLabels(){
        for(Label l : labels){
            if(!search.getText().equals("")&&l.getText().toLowerCase().contains(search.getText().toLowerCase())){
                l.setStyle("-fx-border-color: black;" +
                        "-fx-border-width: 2;");
                l.setTextFill(Color.RED);
                l.setFont(Font.font(null, FontWeight.BOLD, 14));
            } else {
                l.setStyle("");
                l.setTextFill(Color.BLACK);
                l.setFont(Font.font(null, FontWeight.NORMAL, 14));
            }
        }
    }
}
