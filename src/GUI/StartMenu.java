package GUI;

import FileIO.CopyDefaultConf;
import Logic.ExecuteShellCommand;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.*;

/**
 * Creates the start window, where you can start/stop psad or do to the psad configuration
 */
public class StartMenu extends Application {

    private static Stage primaryStage;
    // Label with psad status
    private final Label psadStatusLabel = new Label();

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        BorderPane rootNode = new BorderPane(); // creates border pane
        rootNode.setPadding(new Insets(20));

        addContent(rootNode);

        Scene myScene = new Scene(rootNode, 700, 500); // creates scene with the

        primaryStage.setScene(myScene); // sets scene to the stage
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show(); // shows stage
        primaryStage.setTitle("PSAD Configuration");

        // File for checking if this program starts for the first time
        File sharedProperties = new File("firstStart");

        if (!sharedProperties.exists()) {
            sharedProperties.createNewFile();
            new ErrorMessage().start(primaryStage, "It is the first start of this program. psad.conf will be changed for default.");
            CopyDefaultConf.copyDefaultConf();
        }
    }

    /**
     * Adds content to the root node, for displaying them on the screen
     *
     * @param rootNode - node for adding content
     */
    private void addContent(BorderPane rootNode) {

        // Title lable
        Label myLable = new Label("PSAD Configuration");
        myLable.setMinHeight(50);
        myLable.setFont(new Font(40));

        // Label with psad status
        setPSADStatus();
        psadStatusLabel.setMinHeight(40);
        psadStatusLabel.setFont(new Font(30));

        // Button for staring psad
        Button startButton = new Button("Start PSAD");
        startButton.setFont(new Font(20));
        startButton.setOnAction((event) -> {
            if (ExecuteShellCommand.startPsad()) {
                setPSADStatus();
            } else {
                System.out.println("Error");
            }
        });

        // Button for stoping psad
        Button stopButton = new Button("Stop PSAD");
        stopButton.setOnAction((event) -> {
            if (ExecuteShellCommand.stopPsad()) {
                setPSADStatus();
            } else {
                System.out.println("Error");
            }
        });
        stopButton.setFont(new Font(20));

        // Button for opening psad configuration window
        Button configButton = new Button("Configure PSAD");
        configButton.setFont(new Font(20));
        configButton.setOnAction((event -> {
            try {
                new GUI().start(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        // Vertical Box for all the elements
        VBox main = new VBox(5);
        main.setAlignment(Pos.CENTER);

        // Horizontal box for buttons
        HBox buttons = new HBox(5);
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(startButton, stopButton, configButton); // add elements to HBox

        // Title image
        ImageView psadImage = new ImageView();
        psadImage.setImage(new Image(getClass().getResourceAsStream("psad.png")));
        rootNode.setTop(psadImage);

        main.getChildren().addAll(psadImage, myLable, psadStatusLabel, buttons); // add elements to VBox
        rootNode.setCenter(main);
    }

    /**
     * Sets color psad status to the label, green if the psad is active and otherwise red
     */
    private void setPSADStatus() {
        try {
            String status = ExecuteShellCommand.getPsadStatus();
            if (status.equals("active")) {
                psadStatusLabel.setTextFill(Color.web("#00FF00"));
                psadStatusLabel.setText("PSAD is active");
            } else {
                psadStatusLabel.setTextFill(Color.web("#FF0000"));
                psadStatusLabel.setText("PSAD is inactive");
            }
        } catch (Exception e) {
            try {
                new ErrorMessage().start(primaryStage, "There is no PSAD.");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }
}
