PSAD - The Port Scan Attack Detector, installed on Linux OS.
For creating GUI I've used JavaFX.
psad.conf - main psad configuration file.
That's why I develop the program, that parses this file and provides graphic interface for configuring 41 popular parameters. Search method 
allows finding parameters with its name. Other configurations you can change in text format.
The main problem was that standard psad.conf file is hard for parsing. That's why, if the program starts the first time on computer, new psad.conf, with sorted parameters and deleted comments, is copied and replacing old one.
Next problem was determining the first start. It was solved by adding file "firstStart" to the program directory, after replacing psad.conf file.
For full functionality of program build .jar file and run it with su privileges.
